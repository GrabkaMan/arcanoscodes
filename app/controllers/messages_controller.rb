class MessagesController < ApplicationController

  def show_resend
    respond_to do |format|
      format.html
    end
  end

  def resend
    users = User.where(received: false)

    user_service = UserService.new
    client = WykopApi::connection
    request = Wykop::Operations::Request.new(client)

    @users_with_messages = []

    if client.login
      logger.info "Auth user: #{client.info}"
      response = ""

      users.each do |user|
        logger.info "Trying to resend message to User: #{user.name}"
        response = request.execute(request.replace_url({
          :banana => 'pm',
          :potato => 'sendmessage',
          :param1 => user.name } ),{ body: params[:message_to_users]+ "\n" + user.code})
        logger.info "Response from Wykop.pl: #{response}"
        modified_user = user_service.change_user(user, nil, response)
        @users_with_messages << modified_user if modified_user.received
      end
    end

    respond_to do |format|
      format.html
    end
  end

  def upload_csv
    respond_to do |format|
      format.html
    end
  end

  def show_users_status
    users = User.all
    @all_users_with_keys = users.where(received: true)
    @all_users_without_keys = users.where(received: false)

    respond_to do |format|
      format.html
    end
  end

  def send_messages
    file = params[:file]
    file_path = file.tempfile.path
    client = WykopApi::connection
    request = Wykop::Operations::Request.new(client)
    user_service = UserService.new

    @users_with_messages = []

    if client.login
      logger.info "Auth user: #{client.info}"
      CsvReader.read_csv_data(file_path) do |user_key_row|
        user = User.find_by_name(user_key_row.user)
        response = ""
        if user.blank? || !user.received
          logger.info "Trying to send message to User: #{user_key_row.user}"
          response = request.execute(request.replace_url({
           :banana => 'pm',
           :potato => 'sendmessage',
           :param1 => user_key_row.user } ),{ body: params[:message_to_users]+ "\n" + user_key_row.key})
          logger.info "Response from Wykop.pl: #{response}"
        end
        modified_user = user_service.change_user(user, user_key_row, response)
        @users_with_messages << modified_user if modified_user.received
      end
    end

    respond_to do |format|
      format.html
    end
  end
end
