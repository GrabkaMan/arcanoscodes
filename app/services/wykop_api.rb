class WykopApi

  def self.connection
    Rails.logger.info "ENV['WYKOP_APP_KEY']: #{ENV['WYKOP_APP_KEY']}"
    Rails.logger.info "ENV['WYKOP_APP_SECRET']: #{ENV['WYKOP_APP_SECRET']}"
    Rails.logger.info "ENV['WYKOP_LOGIN']: #{ENV['WYKOP_LOGIN']}"
    Rails.logger.info "ENV['WYKOP_APP_CONN']: #{ENV['WYKOP_APP_CONN']}"
    
    Wykop::Client.new({
                          app_user_key: ENV['WYKOP_APP_KEY'],
                          app_user_secret: ENV['WYKOP_APP_SECRET'],
                          app_username: ENV['WYKOP_LOGIN'],
                          app_generated_key: ENV['WYKOP_APP_CONN'],
                          api_host: 'http://a.wykop.pl' })
  end
end