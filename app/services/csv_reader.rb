class CsvReader

  def initialize
    @user_keys = []
  end

  def self.read_csv_data(path, &block)
    CSV.foreach(path, headers: true) do |row|
      yield UserKeyRow.new(row["user"], row["key"])
    end
  end

  private

  def self.root_path
    Rails.root.join('lib', 'users_and_keys.csv')
  end

end