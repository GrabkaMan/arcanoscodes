class UserService

  def change_user(user, user_key_row, response)
    result = response.kind_of?(Array) && response[0]

    if user.blank?
      User.create({name: user_key_row.user, code: user_key_row.key, received: result, status_message: response})
    else
      if user.received
        user
      else
        user.status_message = result ? nil : response
        user.received = result
        user.save
        user
      end
    end
  end

end