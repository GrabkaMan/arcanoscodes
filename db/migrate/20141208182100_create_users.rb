class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :code
      t.boolean :received

      t.timestamps
    end
  end
end
